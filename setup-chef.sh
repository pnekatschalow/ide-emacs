#!/bin/sh -e

# install Chef Development Kit
if ! [ -x /usr/bin/chef ]; then
    curl -SL https://opscode-omnibus-packages.s3.amazonaws.com/ubuntu/12.04/x86_64/chefdk_0.3.5-1_amd64.deb -o /tmp/chefdk.deb
    dpkg -i /tmp/chefdk.deb
    rm /tmp/chefdk.deb
fi

# install vagrant Chef support
if [ -x /usr/bin/vagrant ]; then
    vagrant plugin install vagrant-omnibus
    vagrant plugin install vagrant-berkshelf
fi
