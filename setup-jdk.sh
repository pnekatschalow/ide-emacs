#!/bin/sh -e

version=8 # java version
update=60 # update number
build=27 # build number

# download JDK
# see https://ivan-site.com/2012/05/download-oracle-java-jre-jdk-using-a-script/
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" -O /tmp/jdk.tgz "http://download.oracle.com/otn-pub/java/jdk/${version}u${update}-b${build}/jdk-${version}u${update}-linux-x64.tar.gz"

# extract and move to /usr/lib/jvm
tar -C /tmp -xzf /tmp/jdk.tgz
rm /tmp/jdk.tgz
mkdir -p /usr/lib/jvm
base_dir="/usr/lib/jvm/jdk1.${version}.0_${update}"
mv /tmp/jdk1.${version}.0_${update} $base_dir

# setup links
update-alternatives --install "/usr/bin/java" "java" "${base_dir}/bin/java" 9999
update-alternatives --install "/usr/bin/javac" "javac" "${base_dir}/bin/javac" 9999
update-alternatives --install "/usr/bin/javaws" "javaws" "${base_dir}/bin/javaws" 9999
chmod a+x /usr/bin/java
chmod a+x /usr/bin/javac
chmod a+x /usr/bin/javaws

# install maven
apt-get update
apt-get install -y maven
