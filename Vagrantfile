# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

# Generate VNC encrypted pass code from VNC_PASS_WORD environment variable.
if ENV.has_key?('VNC_PASS_WORD')
  VNC_PASS_CODE = %x(python vncpasswd.py "#{ENV['VNC_PASS_WORD']}")
else
  VNC_PASS_CODE = '5ab2cdc0badcaf13'
end

# Port number used on the host to map VNC.
VNC_HOST_PORT = ENV['VNC_HOST_PORT'] || 5901

# VM instance name.
VM_NAME = ENV['VM_NAME'] || "ide-emacs"

# VM memory size.
VM_MEMORY = ENV['VM_MEMORY'] || 4096

# VM CPU count.
VM_CPUS = ENV['VM_CPUS'] || 4

# shared directory configurations
if ENV.has_key?('VM_SHARE')
  VM_SHARE = ENV['VM_SHARE']
  VM_MOUNT = ENV['VM_MOUNT'] || "/share"
end

# primary user name
USER_NAME = ENV['USER_NAME'] || "ezhu"

# security keys to install
if ENV.has_key?('USER_KEY_RSA')
  USER_KEY_RSA = ENV['USER_KEY_RSA']
end
if ENV.has_key?('USER_KEY_PGP')
  USER_KEY_PGP = ENV['USER_KEY_PGP']
end


Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  # VMWare based Ubuntu 14.04
  config.vm.box = "chef/ubuntu-14.04"

  # VM configuration
  config.vm.hostname = VM_NAME
  config.vm.provider "vmware_workstation" do |vm|
    vm.vmx["vhv.enable"] = "TRUE"
    vm.vmx["memsize"] = VM_MEMORY
    vm.vmx["numvcpus"] = VM_CPUS
  end

  # map in share folders
  if defined? VM_SHARE
    config.vm.synced_folder VM_SHARE, VM_MOUNT
  end

  # copy over security keys
  if defined? USER_KEY_RSA
    config.vm.provision :file, source: USER_KEY_RSA, destination: "/tmp/key.rsa"
  end
  if defined? USER_KEY_PGP
    config.vm.provision :file, source: USER_KEY_PGP, destination: "/tmp/key.pgp"
  end

  # setup user
  config.vm.provision :shell, path: "setup-user.sh", args: USER_NAME

  # install git
  config.vm.provision :file, source: "gitconfig", destination: "/tmp/gitconfig"
  config.vm.provision :shell, inline: "apt-get install -y git; mv /tmp/gitconfig /home/#{USER_NAME}/.gitconfig; chown #{USER_NAME}:#{USER_NAME} /home/#{USER_NAME}/.gitconfig"

  # copy over the vncserver upstart script
  config.vm.provision :file, source: "vncserver", destination: "/tmp/vncserver"

  # copy over the emacs scripts and fonts
  config.vm.provision :file, source: "lisp", destination: "/tmp"
  config.vm.provision :file, source: "fonts", destination: "/tmp"

  # make sure curl is installed
  config.vm.provision :shell, inline: "apt-get install -y curl"

  # install emacs 24.4
  config.vm.provision :shell, path: "setup-emacs.sh", args: USER_NAME

  # setup VNC server and X11 environment
  config.vm.provision :shell, path: "setup-vnc.sh", args: [USER_NAME, VNC_PASS_CODE]

  # install Java development environment
  config.vm.provision :shell, path: "setup-jdk.sh"

  # setup vagrant
  config.vm.provision :shell, path: "setup-vagrant.sh"

  # setup docker
  config.vm.provision :shell, path: "setup-docker.sh"

  # forward VNC port
  config.vm.network "forwarded_port", guest: 5901, host: VNC_HOST_PORT

end
