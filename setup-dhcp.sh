#!/bin/sh -e

# This script will install NTP, DHCP, and DNS server packages. It then links DHCP server with DNS
# service, so that leased IP addresses are automatically added into the local DNS table. The auto
# DNS linking works only if the DHCP client advertised a device name in its DHCP request. Because
# this machine will be providing DHCP service at the indicated interface, this script also enables
# static IP configuration for the indicated interface.
#
# TODO: support for multiple interfaces.
#
# Usage:   setup-dhcp.sh <domain> <iface> <network> <netmask>
# Example: setup-dhcp.sh home.local eth0 10.0.0.0 255.255.255.0

domain=$1;  : ${domain:="home.local"} # default DNS domain is home.local
iface=$2; : ${iface:="eth0"} # default iface is eth0
network=$3; : ${network:="10.0.0.0"} # default network is 10.0.0.0
netmask=$4; : ${netmask:="255.255.255.0"} # default netmask is 255.255.255.0

# build network configuration
address=`echo $network | sed 's/[0-9]*$/2/'`
network=`echo $network | sed 's/[0-9]*$/0/'`
broadcast=`echo $network | sed 's/[0-9]*$/255/'`
gateway=`echo $network | sed 's/[0-9]*$/1/'`
arpa=`echo $network | awk -F. '{printf "%s.%s.%s", $3, $2, $1}'`
dhcp_start=`echo $network | sed 's/[0-9]*$/100/'`
dhcp_end=`echo $network | sed 's/[0-9]*$/254/'`
host=`hostname -s`

# make sure iface exist
iface_file=/etc/network/interfaces
echo -n "Checking $iface exists in $iface_file ... "
grep "auto $iface" $iface_file > /dev/null
echo "OK"

# install NTP time service
echo -n "Installing NTP server packages ... "
apt-get update > /dev/null
apt-get purge -y ntpdate > /dev/null 2>&1
apt-get install -y --no-install-recommends ntp > /tmp/out.$$ 2>&1
echo "OK"

# install DHCP service
echo -n "Installing DHCP server packages ... "
apt-get install -y --no-install-recommends isc-dhcp-server bind9 dnsutils > /tmp/out.$$ 2>&1
echo "OK"

# Enable DNS caching in /etc/bind/named.conf.options
conf_file="/etc/bind/named.conf.options"
echo -n "Enabling DNS caching in $conf_file ... "
cat > /tmp/out.$$ <<EOF
	allow-query { any; };
	forwarders {
		8.8.8.8;
		8.8.4.4;
	};
EOF
sed '/allow-query .*;$/d' $conf_file |sed '/forwarders {/,/};/d' > /tmp/conf.$$
sed "/directory .*;$/r /tmp/out.$$" /tmp/conf.$$ > $conf_file
rm /tmp/out.$$
echo "OK"

# configure /etc/bind/named.conf.local
conf_file="/etc/bind/named.conf.local"
echo -n "Configuring $conf_file ... "
dnssec-keygen -K /tmp -r /dev/urandom -a HMAC-MD5 -b 128 -n USER DHCP_UPDATER > /dev/null
secret=`grep 'Key:' /tmp/Kdhcp_updater.*.private |sed 's/Key: //'`
rm Kdhcp_updater.*
cat > $conf_file <<EOF
key DHCP_UPDATER {
	algorithm HMAC-MD5.SIG-ALG.REG.INT;
	secret "$secret";
};
zone "$domain" {
	type master;
	file "/var/lib/bind/db.${domain}";
	allow-update { key DHCP_UPDATER; };
};
zone "${arpa}.in-addr.arpa" {
	type master;
	file "/var/lib/bind/db.${arpa}";
	allow-update { key DHCP_UPDATER; };
};
EOF
echo "OK"

# generating forward db file
db_file="/var/lib/bind/db.$domain"
echo -n "Generating forward DNS file $db_file ... "
cat > $db_file <<EOF
\$TTL    604800
@       IN      SOA     ${host}.${domain}. admin.${domain}. (
                              2         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
@       IN      NS      ${host}.${domain}.
@       IN      A       ${address}
@       IN      AAAA    ::1
${host} IN      A       ${address}
EOF
echo "OK"

# generating reverse db file
db_file="/var/lib/bind/db.$arpa"
echo -n "Generating reverse DNS file $db_file ... "
cat > $db_file <<EOF
\$TTL    604800
@       IN      SOA     ${host}.${domain}. admin.${domain}. (
                              2         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
@       IN      NS      ${host}.${domain}.
2       IN      PTR     ${host}.${domain}.
EOF
echo "OK"

# generating /etc/dhcp/dhcpd.conf
conf_file="/etc/dhcp/dhcpd.conf"
echo -n "Generating DHCP server configuration file $conf_file ... "
cat > $conf_file <<EOF
ddns-update-style interim;
ignore client-updates;
ddns-domainname "${domain}.";
ddns-rev-domainname "in-addr.arpa.";
option domain-name "${domain}";
option broadcast-address ${broadcast};
option domain-name-servers ${address};
option ntp-servers ${address};
authoritative;
key DHCP_UPDATER {
    algorithm HMAC-MD5.SIG-ALG.REG.INT;
    secret "${secret}";
};
zone ${domain}. {
    primary 127.0.0.1;
    key DHCP_UPDATER;
}
zone ${arpa}.in-addr.arpa. {
    primary 127.0.0.1;
    key DHCP_UPDATER;
}
subnet ${network} netmask ${netmask} {
    option routers ${gateway};
    range ${dhcp_start} ${dhcp_end};
}
EOF
echo "OK"

# update file permissions
echo -n "Updating file permissions ... "
chown bind:bind /var/lib/bind/*
chown dhcpd:dhcpd /etc/dhcp/dhcpd.conf
chmod o-r /etc/bind/named.conf.local
#chmod o-r /etc/dhcp/dhcpd.conf
echo "OK"

# setup static IP
echo -n "Setting $iface to static IP $address ... "
cat > /tmp/out.$$ <<EOF 
iface $iface inet static
	address $address
	network $network
	netmask $netmask
	broadcast $broadcast
	dns-search $domain
	dns-nameservers $address 8.8.8.8 8.8.4.4
	gateway $gateway
EOF
iface_tmp=/tmp/interfaces.$$
sed "/iface $iface inet static/,/gateway.*/d" $iface_file > $iface_tmp # remove any exiting static IP
sed "/iface $iface inet dhcp/d" $iface_tmp |sed "/auto $iface/r /tmp/out.$$" > $iface_file
rm /tmp/out.$$; rm $iface_tmp
echo "OK"

# restart network services
echo "Resetting network interface $iface ... "
ifdown $iface
ifup $iface
service bind9 restart
service isc-dhcp-server restart
echo "All Finished!!!"
