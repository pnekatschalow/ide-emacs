#!/bin/sh -e

# install VirtualBox
if ! [ -x /usr/bin/virtualbox ]; then
    echo "deb http://download.virtualbox.org/virtualbox/debian trusty contrib" >> /etc/apt/sources.list
    wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | apt-key add -
    apt-get update
    apt-get install -y linux-headers-$(uname -r) virtualbox-4.3 dkms
fi

# install Debian package downloaded from web
if ! [ -x /usr/bin/vagrant ]; then
    curl -SL https://dl.bintray.com/mitchellh/vagrant/vagrant_1.6.5_x86_64.deb -o /tmp/vagrant.deb
    dpkg -i /tmp/vagrant.deb
    rm /tmp/vagrant.deb
fi
