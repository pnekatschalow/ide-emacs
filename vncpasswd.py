import d3des

def get_vnc_enc(password):
    if (len(password)>8):
        return get_vnc_enc(password[:8]) + get_vnc_enc(password[8:])

    passpadd = (password + '\x00'*8)[:8]
    strkey = ''.join([ chr(x) for x in d3des.vnckey ])
    ekey = d3des.deskey(strkey, False)
    ctext = d3des.desfunc(passpadd, ekey)
    return ctext.encode('hex')

if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        print get_vnc_enc(sys.argv[1])
    else:
        print 'usage: %s <password>' % sys.argv[0]
